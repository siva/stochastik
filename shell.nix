with import <nixpkgs> {};
mkShell {
  name = "math";
  buildInputs = [
    (python311.withPackages (ps: with ps; [
      matplotlib
      notebook
      numpy
   ]))
  ];
}

